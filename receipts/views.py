from django.shortcuts import render, redirect
from receipts.models import ExpenseCategory, Account, Receipt
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm

# Create your views here.
# Homepage: List all receipts


@login_required
def receipts_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)

    context = {
        "receipts": receipts
    }

    return render(request, "receipts/list.html", context)

# View to Create New Receipt


@login_required
def add_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()

    context = {
        "form": form
    }

    return render(request, "receipts/add.html", context)

# Expense Category List View


@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)

    context = {
        "categories": categories
    }

    return render(request, "receipts/categories.html", context)

# Create new Expense Category


@login_required
def create_expense_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()

    context = {
        "form": form
    }

    return render(request, "receipts/add_expense.html", context)

# Account List View


@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)

    context = {
        "accounts": accounts
    }

    return render(request, "receipts/accounts.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()

    context = {
        "form": form
    }

    return render(request, "receipts/add_account.html", context)
