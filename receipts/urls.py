from django.urls import path
from receipts.views import receipts_list, add_receipt, category_list, account_list, create_expense_category, create_account

urlpatterns = [
    path("", receipts_list, name="home"),
    path("create/", add_receipt, name="create_receipt"),
    path("categories/", category_list, name="category_list"),
    path("accounts/", account_list, name="account_list"),
    path("categories/create/", create_expense_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account")

]
